import classes.database;
import classes.Gui;

public class App {

   public static void main(String[] args){
      // instantiate classes
      database db = new database();
      Gui gui = new Gui();
      // retrieve all records
      String[][] records = db.getPersoonRecords();
      // generate the first panel, te rest is done in the Gui class
      gui.createOverviewPanel(records);
   }  
}