package classes;

import javax.swing.*;
import java.awt.event.*;  

public class Gui {
    // createOverviewPanel(): This function starts directly from the main function (App.java)
    public static void createOverviewPanel(String[][] data){
        JFrame frame = new JFrame("Persoon overzicht");
        DefaultListModel<String> l1 = new DefaultListModel<>();
        for (int i = 0; i < data.length; i++) {
           if (data[i][0] != null) {
              System.out.println(data[i][0] + data[i][1]);
              l1.addElement(data[i][0] + " " + data[i][1]);
           }
        }
        JList<String> list_1 = new JList<>(l1);  
        JButton b1 = new JButton("Add another person");
        JButton b2 = new JButton("Refresh");
        b1.addActionListener(new ActionListener(){  
           public void actionPerformed(ActionEvent e){  
                createAddPanel(frame); 
                }
            }
        );
        b2.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    frame.setVisible(false);
                    frame.setVisible(true);
                }
             }
         );
        // set x/y asses & width/height
        b1.setBounds(220, 350, 200 , 50);
        b2.setBounds(20, 350, 150, 50);
        list_1.setBounds(10, 10, 150, 100);
        // add the elements to the frame
        frame.add(b1);
        frame.add(b2);
        frame.add(list_1);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // Quit the application when the JFrame is closed
        frame.setSize(450,450);  
        frame.setLayout(null);  
        frame.setVisible(true); // Show the window   
    }

    // createAddPanel(): This function is triggered once the user clicks on the 'Add another person' button
    private static void createAddPanel(JFrame mainFrame){
        JFrame frame = new JFrame("voeg toe");
        JButton b1 = new JButton("submit person");
        JTextField field1 = new JTextField();
        JTextField field2 = new JTextField();
  
        b1.addActionListener(new ActionListener(){  
            public void actionPerformed(ActionEvent e){  
                    boolean result = database.insertPersoonRecords(field1.getText(), field2.getText());
                    if (result) {
                        System.out.println("Query was succesfully executed!");
                    } else {
                        System.out.println("Failed to execute query");
                    }
                }
            }
        );

        // set x/y asses & width/height
        b1.setBounds(20, 200, 200 , 50);
        
        field1.setText("first name");
        field2.setText("last name");
  
        field1.setBounds(20, 10, 200, 40);
        field2.setBounds(20, 50, 200, 40);
  
        frame.add(b1);
        frame.add(field1);
        frame.add(field2);
        frame.setSize(250,300);  
        frame.setLayout(null);  
        frame.setVisible(true); //Show the window  
    }

}
