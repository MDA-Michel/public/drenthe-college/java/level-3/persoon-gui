package classes;
//
import java.sql.*;
import java.util.Arrays;  

public class database {
    // getPersoonRecords(): retrieves all the first and lastnames from the persoon table which resides in the java db
    public static String[][] getPersoonRecords(){
        int rows = 0;
        try {  
            Class.forName("com.mysql.cj.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java","root",""); // 'java' is the db
            Statement stmt=con.createStatement();  
            ResultSet rs1 = stmt.executeQuery("SELECT COUNT(*) FROM Persoon");
            while (rs1.next()) {
                rows = rs1.getInt(1);
            }
            // set fixed array based on the number of rows
            String records[][] = new String[rows][];

            // get all records
            ResultSet rs2 = stmt.executeQuery("SELECT * from Persoon");
            int i = 0;  
            while (rs2.next()) {
                String[] data = {rs2.getString(1), rs2.getString(2)};
                Arrays.copyOf(records, records.length + 1);
                records[i] = data;
                i++; 
            }
            con.close(); 
            return records;
        }
        catch(Exception e){ 
            System.out.println(e);
        }
        return new String[0][0];
    } 
    
    // insertPersoonRecords(): generates an insert query in order to add another record to the table
    public static boolean insertPersoonRecords(String firstname, String lastname){
        try {  
            Class.forName("com.mysql.cj.jdbc.Driver");  
            Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/java","root",""); // java is the db
            PreparedStatement stmt=con.prepareStatement("INSERT INTO Persoon (voornaam, achternaam) VALUES (?, ?)");  
            stmt.setString(1, firstname);
            stmt.setString(2, lastname);
            stmt.executeUpdate();
            con.close(); 
            return true;
        }  catch (Exception e) { 
            System.err.println(e.getMessage());
            return false; 
        } 
    }
}  